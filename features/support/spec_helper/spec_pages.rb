module SpecPages

  def home
    SpecPages::Home.new
  end

  def home_boarding_search
    SpecPages::HomeBoardingSearch.new
  end

  def login_modal
    SpecPages::LoginModal.new
  end

  def register_modal
    SpecPages::RegisterModal.new
  end

  def meus_herois
    SpecPages::MeusHerois.new
  end

  def pre_encontro
    SpecPages::PreEncontro.new
  end

  def search_boarding_modal
    SpecPages::SearchBoardingModal.new
  end

  def search_result
    SpecPages::SearchResult.new
  end

  def search
    SpecPages::Search.new
  end

  def host_profile_pictures
    SpecPages::HostProfilePictures.new
  end

  def host_profile_user_info
    SpecPages::HostProfileUserInfo.new
  end

  def host_profile_reviews
    SpecPages::HostProfileReviews.new
  end

  def host_profile
    SpecPages::HostProfile.new
  end

end