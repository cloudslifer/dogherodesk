require 'capybara-screenshot/rspec'
require 'rspec'


module AssertAndPrint include RSpec::Matchers

  def expecting(condition, expected, description, qty_screenshots=1)
    if condition == expected
      status = 'passed'
      log_info description
    else
      log_error description
      status = 'failed'
    end

    take_screenshot_steps description, status, qty_screenshots


    expect(condition).to eql(expected)

  end

  def tracing(description, screenshot = false)
    log_info description
    if screenshot
      take_screenshot_steps description, 'passed'
    end

  end

  def tracing_error(description, screenshot = true)
    log_error description

    unless screenshot
      take_screenshot_steps description, 'failed'
    end


    expect(true).to eql(false)
  end

  private



  def log_warning warning_msg
    msg = "[WARNING]: #{warning_msg}"
    puts msg
  end

  def log_info info_msg
    msg = "[INFO]: #{info_msg}"
    puts msg
  end

  def log_error error_msg
    if MOBILE
      plat = "Mobile"
    else
      plat = "Desktop"
    end

    msg = "[ERRO]: #{error_msg}
          *** Detalhes do erro ***
          Data: #{Time.now.strftime('%d/%m/%Y').to_s}
          Horário: #{Time.now.strftime('%H:%M:%S')}
          Site: #{$current_host}
          URL do erro: #{page.current_url}
          Host: #{$ip_host}
          Plataforma: #{plat}
          Navegador: #{BROWSER}
          Ambiente: #{TEST_ENVIRONMENT.upcase}
          ************************************"
    $error_log = msg
    expect(true).to eql(false)
  end


end