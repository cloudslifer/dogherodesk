require 'capybara-screenshot/rspec'

module Screenshots

  @@file_path

  def initialize
    timer_path = Time.now.strftime('%Y_%m_%d')
    @@file_path = "screenshots/#{timer_path}/#{$scenario.name}/"
    FileUtils.mkdir_p(@@file_path) unless File.exist?(@@file_path)

  end

  def take_screenshot_steps (description, status='passed', qty_screenshots=1)
    $count_steps += 1

    if qty_screenshots == 1
      img = Capybara.page.save_screenshot "#{@@file_path}/step_#{$count_steps}_#{description}_#{status}.png"
      embed(img, 'image/png')
    else
      for i in 1..$screenshots do
        img = Capybara.page.save_screenshot "#{@@file_path}/step_#{$count_steps}_#{i}_#{description}_#{status}.png"
        embed(img, 'image/png')
        scroll_page_down
      end
    end
  end

  def take_final_screenshot(name)
    img = Capybara.page.save_screenshot "#{@@file_path}/#{name.to_s}_final.png"
    img
  end
end
