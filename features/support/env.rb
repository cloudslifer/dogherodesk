require 'rubygems'
require 'rspec'
require 'selenium-webdriver'
require 'capybara/cucumber'
require 'pry'
require 'capybara-screenshot/rspec'
require 'site_prism'
require 'base64'
require 'capybara/angular'
require 'faker'

Dir[File.join(File.dirname(__FILE__), 'spec_helper/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'helpers/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'data/**/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'assert/*.rb')].sort.each {|file| require file}
Dir[File.join(File.dirname(__FILE__), 'elements/**/*.rb')].sort.each {|file| require file}

LINKS ||= YAML.load_file('features/support/data/setup/links.yml')

ENV['MOBILE'].nil? ? MOBILE = false : MOBILE = true
ENV['REMOTE'].nil? ? REMOTE = false : REMOTE = true

BROWSER ||= ENV['BROWSER']
LARGURA ||= ENV['LARGURA'] ||= '360'
ALTURA ||= ENV['ALTURA'] ||= '640'

ENV['TEST_ENVIRONMENT'] ? TEST_ENVIRONMENT = ENV['TEST_ENVIRONMENT'] : 'não informado'
COUNTRY ||= ENV['COUNTRY']



Capybara.register_driver :selenium do |driver|

  if REMOTE
    remote_driver driver
  else
    local_driver driver
  end

end

def local_driver driver
  caps = Selenium::WebDriver::Remote::Capabilities.chrome
  caps['pageLoadStrategy'] = 'normal'
  if MOBILE
    mobile_emulation = {
        "deviceMetrics" => { "width" => LARGURA, "height" => ALTURA, "pixelRatio" => 3.0 },
        "userAgent" => "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
    }

    caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => mobile_emulation)

    options = Selenium::WebDriver::Chrome::Options.new(args: ["window-size=#{LARGURA},#{ALTURA}", 'ignore-certificate-errors', 'disable-popup-blocking','disable-web-security','disable-infobars'])
  else
    options = Selenium::WebDriver::Chrome::Options.new(args: ['window-size=1280,1024", ignore-certificate-errors', 'disable-popup-blocking','disable-web-security','disable-infobars'])
  end


  Capybara::Selenium::Driver.new(driver, browser: :chrome, options: options,
                                 desired_capabilities: caps)

end



def remote_driver driver
  hub = "http://localhost:4445/wd/hub"


  if BROWSER == 'chrome'

    options = Selenium::WebDriver::Chrome::Options.new(args: ["window-size=1280,1024",'headless', 'disable-gpu', '--no-sandbox', 'ignore-certificate-errors', 'disable-popup-blocking','disable-web-security','disable-infobars'])
    options.headless!

    if MOBILE
      options = Selenium::WebDriver::Chrome::Options.new(args: ["window-size=#{LARGURA},#{ALTURA}", 'headless', 'disable-gpu', '--no-sandbox', 'ignore-certificate-errors', 'disable-popup-blocking','disable-web-security','disable-infobars'])
      mobile_emulation = {
          "deviceMetrics" => { "width" => LARGURA, "height" => ALTURA, "pixelRatio" => 3.0 },
          "userAgent" => "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"
      }

      caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => mobile_emulation)
    end
  else
    #caps = Selenium::WebDriver::Remote::Capabilities.firefox
    options = Selenium::WebDriver::Firefox::Options.new(args: ['ignore-certificate-errors', 'disable-popup-blocking','disable-web-security','disable-infobars'])

  end

=begin
    caps = {}
    caps['zal:name'] = "#{ENV['TEST_NAME']}_#{Time.now.strftime('%Y_%m_%d').to_s}_#{Time.now.strftime('%H:%M:%S')}"
    caps['zal:build'] = BROWSER
    caps['zal:tz'] = 'America/Sao_Paulo'

=end

    if MOBILE
      Capybara::Selenium::Driver.new(driver, :browser => BROWSER.to_sym, options: options, desired_capabilities: caps,  url: hub)
    else
      Capybara::Selenium::Driver.new(driver, :browser => BROWSER.to_sym, options: options, url: hub)
    end

end


Capybara.configure do
  Capybara.default_driver = :selenium
  #Capybara.page.driver.browser.manage.window.maximize unless MOBILE
end

World(SpecPages, ClickEl, SendKeys, FindEl, WaitElement, Check, RetryAction, Screenshots, AssertAndPrint, TestData, Select, WaitPage, CommonHelpers)