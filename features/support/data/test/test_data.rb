module TestData


  def load_address_file
    addresses_file = YAML.load_file('features/support/data/test/addresses.yml')
    addresses_file
  end


  def get_address_from_yml
    addresses = load_address_file['addresses'].values
    addresses
  end

  def get_specific_address_type_from_yml address_type
    address = load_address_file['addresses'][address_type.downcase]
    address
  end

  def default_password password
    if password == 'default'
      password = 'Dh1234@'
      password
    else
      password
    end
  end

  def random_data (text, type)
    type.upcase!
    text.upcase!
    if text == 'RANDOM'
      case type
      when 'NAME'
        text = Faker::Name.first_name
      when 'LAST_NAME'
        text = Faker::Name.last_name
      when 'FULL_NAME'
        text = Faker::Name.name
      when 'DOG_NAME'
        text =  Faker::Games::Pokemon.name
      when 'VERB'
        text = Faker::Verb.base
      when 'CPF'
        text = Faker::CPF.number
      when 'DAY'
        text = rand(01...29).to_s
      when 'MONTH'
        text = rand(01...12).to_s
      when 'FULL_YEAR_PAST'
        text = rand(1960...1996).to_s
      when 'FULL_YEAR_FUTURE'
        text = rand(2019...2099).to_s
      when 'FULL_YEAR_ALL'
        text = rand(1960...2099).to_s
      when 'YEAR_PAST'
        text = rand(60...96).to_s
      when 'YEAR_FUTURE'
        text = rand(19...99).to_s
      when 'YEAR_ALL'
        text = rand(60...99).to_s
      when 'PHONE_NUMBER'
        text = rand(111111111...999999999).to_s
      when 'DDD'
        text = rand(10...99).to_s
      when 'EMAIL'
        text = 'qa_floquinho_desk_' + Faker::Number.between(1, 10000000).to_s + '@mailinator.com'
      when 'ADDRESS'
        text = get_address_from_yml.sample
      end
    elsif type == 'ADDRESS'
      text = get_specific_address_type_from_yml text
    end
      text
  end

end
