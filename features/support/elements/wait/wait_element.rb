module WaitElement

  def wait_until_el_displayed(finder, el)
    wait_els do
      page.has_selector?(finder,el)
      el = page.find(finder, el)
    end

    el
  end

  def wait_until_child_el_displayed(element_solved, finder, el)
    wait_els do
      page.has_selector?(finder,el)
      el = element_solved.find(finder, el)
    end
    el
  end

  def wait_until_parent_el_displayed(element_solved, finder, el)
    wait_els do
      page.has_selector?(finder,el)
      el = element_solved.ancestor(finder, el)
    end
    el
  end

  def wait_until_el_list_displayed(finder, el)
    wait_els do
       page.has_selector?(finder,el)
       el = page.find_all(finder, el)
       el
    end
  end

  def wait_until_child_el_list_displayed(element_solved, finder, el)
    wait_els do
      element_solved.has_selector?(finder,el)
      el = element_solved.find_all(finder, el)
    end
    el
  end

  def wait_els

      options = {}
      options[Capybara.default_max_wait_time]
      wait = Selenium::WebDriver::Wait.new(options)
      element = wait.until {
        yield
      }

      element unless element.nil?

  end


end