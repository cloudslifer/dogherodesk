module WaitPage

  def wait_page
    sleep 2
    wait_for_page_to_load
  end

  def wait_for_page_to_load(options = {})
    begin
      options[:timeout] ||= 5
      wait = Selenium::WebDriver::Wait.new(options)
      wait.until {Capybara.execute_script('var browserState = document.readyState; return browserState;') == "complete"}
    rescue StandardError
      begin
        wait.until {Capybara.execute_script('return jQuery.active') == "0"}
      rescue StandardError
        Capybara.execute_script('return window.stop()')
      end
    end
  end




end