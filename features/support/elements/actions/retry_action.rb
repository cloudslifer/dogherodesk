module RetryAction


  def retry_action action_name, el_name
    retries = 0

    begin
      yield
    rescue Selenium::WebDriver::Error::ElementClickInterceptedError, Capybara::CapybaraError => e
      retries += 1
      if retries <=2
        retry
      else
        tracing_error "Erro ao #{action_name} no elemento #{el_name}. \n Erro: #{e.message}"
      end

    end


  end


end
