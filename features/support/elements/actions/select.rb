module Select

  def select_el(el, value, options = {})
    retry_action("selecionar", el) do
      element = find_el(el)
      element.select(value, options)
    end
  end

  def select_random_el(el)
    retry_action("selecionar", el) do
      elements = find_all_els(el)
      elements.sample.click
    end
  end

  def select_text_el(el, text)
    retry_action("selecionar", el) do
      elements = find_all_els(el)
      elements.each do |el|
        next unless el.text == text
        el.click
        break
      end
     end
  end

end
