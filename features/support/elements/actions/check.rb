module Check

  def is_visible_el el
    find_el_exists el
  end

  def is_visible_text_el el, text
    bool = false
    text.downcase!
    retry_action("Verifica se existe um elemento com o texto #{text}", el) do
      elements = find_all_els(el)
      elements.each do | element |
        if element.text.downcase == text.downcase
          bool = true
          break
        end
      end
    end
    bool
  end


  def is_visible_child_el element, el
    find_child_el_exists element, el
  end

  def is_visible_list_el (el)
    elements = find_all_els_exists el
    elements.size > 0 ? true : false
  end


  def get_text_el el
    retry_action("receber text", el) do
      find_el(el).text
    end
  end

  def get_attr_el solved_el, attr, el_reference
    retry_action("recebe o atributo #{attr}", el_reference) do
      solved_el[attr.to_sym]
    end
  end


  def is_checked_el el
    element = find_el el
    element.checked?
  end


  def compare_text_el(el, compare_txt)
    text = ''
    retry_action("receber text", el) do
      text = find_el(el).text.downcase
    end

    text == compare_txt.downcase ?  true : false
  end

  def check_page_has_text txt
    page.has_text? txt
  end

  def get_text_solved_el el, el_reference
    retry_action("receber text", el_reference) do
      el.text
    end
  end

  def has_text_el el
    txt = ''
    retry_action("receber text", el) do
      el = find_el(el)
      txt = el.text
    end

    txt.empty? ? false : true
  end

  def has_text_solved_el el, el_reference
    txt = ''
    retry_action("receber text", el_reference) do
      txt = el.text
    end

    txt.empty? ? false : true

  end





end