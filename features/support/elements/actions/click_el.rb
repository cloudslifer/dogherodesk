module ClickEl


  def click_el el
    retry_action("clicar", el) do
      element = find_el(el)
      element.click
    end
  end

  def click_script_el el
    retry_action("clicar", el) do
      find_el(el)
      page.execute_script("document.querySelector('#{@el_locator}').click();")
    end
  end

  def click_random_el el
    retry_action("clicar", el) do
      elements = find_all_els(el)
      elements.sample.click
    end
  end

  def click_random_with_text_el el
    retry_action("clicar", el) do
      elements = find_all_els(el)

      text_elements = []
      elements.each do | el|
        unless el.text.empty?
          text_elements << el
        end
      end

      text_elements.sample.click

    end
  end


  def click_first_el el
    retry_action("clicar", el) do
      elements = find_all_els(el)
      elements.first.click
    end
  end


  def click_text_el el, text
    text.downcase!
    retry_action("clica no elemento com texto #{text}", el) do
      elements = find_all_els(el)
      elements.each do | element |
        if element.text.downcase == text.downcase
          element.click
          break
        end
      end
    end
  end

  def click_solved_el(element, el_description)
    retry_action("clicar", el_description) do
      element.click
    end
  end


  private

  def define_finders(el_name)
    discover = DiscoverElement.new
    discover.discover(el_name)

    @description = discover.description
    @finder = discover.finder
    @el_locator = discover.element
  end



end