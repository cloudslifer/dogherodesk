module SendKeys

  def send_keys_el(el, value)
    retry_action("enviar texto", el) do
      element = find_el(el)
      element.set value
    end
  end

  def send_action_keys_el(el, sym)
    retry_action("enviar texto", el) do
      element = find_el(el)
      element.native.send_keys(sym.to_sym)
    end
  end


  def clear_el el
    retry_action("limpar o texto", el) do
      element = find_el(el)
      element.native.clear
    end
  end



end