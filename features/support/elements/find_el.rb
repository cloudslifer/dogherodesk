module FindEl

  @page_name
  @description
  @finder
  @el_locator

  def define_finders(el_name)
    discover = DiscoverElement.new
    discover.discover(el_name)

    @description = discover.description
    @finder = discover.finder
    @el_locator = discover.element
  end


  def find_el_exists(el_name)
    el = retry_finder_exists el_name do
      wait_until_el_list_displayed @finder, @el_locator
    end

    check_exists el

  end

  def find_child_el_exists(element, el_name)
    el = retry_finder_exists el_name do
      wait_until_child_el_list_displayed element, @finder, @el_locator
    end

    check_exists el
  end

  def find_all_els_exists(el_name)
    el = retry_finder_exists el_name do
      page.find_all(@finder, @el_locator)
    end

    check_exists el
  end

  def check_exists el_result
    if el_result.nil?
      false
    else
      el_result.size>0 ? true : false
    end
  end


  def find_all_els(el_name)
    els = retry_finder el_name do
      wait_until_el_list_displayed @finder, @el_locator
    end

    if check_exists els
      els
    else
      find_el_log_error el_name
    end
  end


  def find_parent_el(element, el_name)
    retry_finder el_name do
      wait_until_parent_el_displayed element, @finder, @el_locator
    end
  end

  def find_child_el(element, el_name)
    retry_finder el_name do
      wait_until_child_el_displayed element, @finder, @el_locator
    end
  end

  def find_child_elements(element, el_name)
    retry_finder el_name do
      wait_until_child_el_list_displayed element, @finder, @el_locator
    end
  end

  def find_el(el_name)
    retry_finder el_name do
      wait_until_el_displayed @finder, @el_locator
    end
  end


  def retry_finder el_name
    define_finders el_name
    retries = 0

    begin
      yield
    rescue Capybara::ElementNotFound => e
      if e.message.include? 'Ambiguous match'
        find_el_ambiguous_match_error el_name
      else
        retries += 1
        if retries <=2
          retry
        else
          find_el_log_error el_name
        end
      end

    end

  end





  def retry_finder_exists el_name
    define_finders el_name
    retries = 0

    begin
      yield
    rescue StandardError
      wait_page
      retries += 1
      if retries <=2
      end
    end

  end

  def find_el_ambiguous_match_error el_name
    tracing_error "Mais de um elemento foi encontrado com esses termos, por favor, use um método específico para tratamento de múltiplos elementos: #{el_name}. Locator: #{@el_locator}. Descrição: #{@description}"
  end

  def find_el_log_error el_name
    tracing_error "Elemento não encontrado: #{el_name}. Locator: #{@el_locator}. Descrição: #{@description}"
  end


end