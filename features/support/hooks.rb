require 'capybara-screenshot/rspec'
require 'capybara/angular'

SitePrism::Page.class_eval do
  include SpecPages, FindEl, ClickEl, SendKeys, WaitElement, Check, RetryAction, AssertAndPrint, Screenshots, TestData, Select, WaitPage, Capybara::Angular::DSL, CommonHelpers
end

Before do | scenario |
  $screenshots = 1
  $scenario = scenario
  $count_steps = 0
end

After do | scenario |
  scenario.passed? ? status = 'x_passed' : status = 'x_failed'


  for i in 1..$screenshots do
    embed take_final_screenshot("#{status}_#{i}"), 'image/png'
    scroll_page_down
  end

  puts $error_log
  send_zalenium_status scenario

  page.execute_script("localStorage.clear()")

end


def send_zalenium_status scenario
  scenario.passed? ? cookie = 'true' : cookie = 'false'
  page.driver.browser.manage.add_cookie(name: 'zaleniumTestPassed', value: cookie)
end

at_exit do
  Capybara.current_session.driver.quit
end


