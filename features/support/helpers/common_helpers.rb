module CommonHelpers

  def scroll_page_down(value = '200')
    page.execute_script("window.scrollBy(0,#{value})", '')
  end

  def scroll_page_up(value = '-250')
    page.execute_script("window.scrollBy(0,-#{value})", '')
  end

  def go_to(url)
    visit url
    wait_page
  end

  def switch_to_new_tab
    sleep 2
    page.driver.browser.switch_to.window page.driver.browser.window_handles[1]
  end

end
