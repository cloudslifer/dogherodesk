Então("verifico que minha quantidade total de heróis é {string}") do |qty|
  expecting(meus_herois.check_heroes_qty(qty), true, "Verifica se a quantidade de heróis é igual a #{qty}")
end

Quando("eu contato o herói {string}") do |hero|
  meus_herois.choose_hero_by_name hero
end

Dado("estou logado com sucesso na tela de meus heróis") do |table|
  table.hashes.each do |row|
    step "faço o login com meu email '#{row[:email]}' e senha '#{row[:password]}'"
    step 'o meu acesso é efetuado com sucesso'
    step 'acesso a tela de meus heróis'
  end
end