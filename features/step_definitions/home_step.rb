Dado("que estou na tela inicial") do
  step 'defino o cookie para desabilitar o bounce exchange'
  home.load

  expecting(home.is_home, true, 'verifica que está na tela inicial')
  #home.close_pop_up
end

E("defino o cookie para desabilitar o bounce exchange") do
  home.load
  Capybara.current_session.driver.browser.manage.add_cookie(name: 'be', value: 'true')

end

Então("o meu acesso é efetuado com sucesso") do
  expecting(home.is_user_logged, true, 'verifica que usuário está logado')
end

Então("valido que meu cadastro foi efetuado com sucesso") do
  expecting(home.check_user_name($user_name), true, 'Verifica que o cadastro foi efetuado com sucesso')
end

Quando("acesso a tela de meus heróis") do
  home.access_my_heroes_page
end

Dado("vou para a tela de busca por anfitrião") do
  home.go_to_boarding_search
end

Quando("abro o modal de login") do
  home.open_login_modal
  tracing('modal de login', true)
end

Quando("efetuo uma busca por um anfitrião com o endereço {string} sem os pets {string}") do | address, pet_size|
  home_boarding_search.boarding_search address, pet_size
end

Quando("efetuo uma busca por um anfitrião com o endereço {string}") do | address|
  home_boarding_search.boarding_search_mobile address
end


Quando("que efetuo uma busca na tela inicial por um anfitrião") do | table |
  table.hashes.each do | row |
    step "que estou na tela inicial"
    if MOBILE
      step "efetuo uma busca por um anfitrião com o endereço '#{row[:address]}'"
    else
      step "efetuo uma busca por um anfitrião com o endereço '#{row[:address]}' sem os pets '#{row[:pet_size]}'"
    end
  end

end


Dado("que estou logado na tela inicial") do |table|
  table.hashes.each do |row|
    step 'que estou na tela inicial'
    step "faço o login com meu email '#{row[:email]}' e senha '#{row[:password]}'"
    step 'o meu acesso é efetuado com sucesso'
  end
end


E("faço o logout") do
  home.do_logout
  expecting(home.check_logged_out, true, 'Verifica que o logout foi feito com sucesso')
end
