
Dado("que estou na página de busca por anfitrião") do
  step 'que estou na tela inicial'
  step 'vou para a tela de busca por anfitrião'
end

E("verifico que a página de busca por anfitrião está sendo apresentada corretamente") do
  step 'fecho o modal da tela de busca'
  expecting(search.search_main_text_validation, false, 'Página de busca apresentada com textos principais corretos')
end

Quando("que estou logado na página de busca por anfitrião") do | table |
  table.hashes.each do |row|
    step 'que estou na tela inicial'
    step "faço o login com meu email '#{row[:email]}' e senha '#{row[:password]}'"
    step 'o meu acesso é efetuado com sucesso'
    step 'vou para a tela de busca por anfitrião'
  end

end

Entao("valido os resultados apresentados") do
  #$screenshots = 4
  step 'verifico que a página de busca por anfitrião está sendo apresentada corretamente'

  error_result = search_result.valid_result
  if error_result == '[]'
    expecting(true, true, "Verifica que o resultado da busca foi apresentado corretamente")
  else
    expecting(false, true, "Erro ao conferir os resultados apresentados. Os seguintes elementos não foram encontrados: #{error_result}")
  end


  error_content= search_result.valid_result_content
  if error_content == '[]'
    expecting(true, true, 'Verifica que o conteúdo da busca está preenchido corretamente')
  else
    expecting(false, true, "Erro ao conferir o conteúdo da busca. Os seguintes elementos não apresentaram nenhum texto, quando deveriam: #{error_content}")
  end

  #$screenshots = 1
end


Quando("efetuo uma busca por um anfitrião no endereço {string} com o tamanho {string}, ambiente {string}, e com filtros adicionais {string}") do | address, pet_size, environment, extra_options|
  search.search(address, pet_size, environment, extra_options)
end

Quando("efetuo uma busca por um anfitrião no endereço {string} com o tamanho {string}, ambiente {string}, com filtros adicionais {string} e contemplando fins de semana") do | address, pet_size, environment, extra_options|
  search.search(address, pet_size, environment, extra_options, true)
end

Quando("efetuo uma busca por um anfitrião no endereço {string}, com filtros adicionais {string} e contemplando fins de semana") do | address, extra_options|
  search.search_mobile(address, extra_options, true)
end
Quando("efetuo uma busca por um anfitrião no endereço {string} e com filtros adicionais {string}") do | address, extra_options|
  search.search_mobile(address, extra_options)
end

Quando("preencho os filtros adicionais: ordenação {string}, tamanho do pet {string}, ambiente {string}, área externa {string}, pets do anfitrião {string}, outros {string}") do | sorting, pet_size, environment, external_area, has_dogs, other_options|
  search.extra_options_mobile(sorting, pet_size, environment, external_area, has_dogs, other_options)
end

Quando("preencho os filtros adicionais: área externa {string}, pets do anfitrião {string}, outros {string}") do | external_area, has_dogs, others|
  search.extra_options(external_area, has_dogs, others )
end

Quando("fecho o modal da tela de busca") do
  search_boarding_modal.close_modal
end

Quando("efetuo uma busca por um anfitrião pelo modal no endereço {string} com o tamanho {string}, gênero {string}, e castrado {string}") do | address, pet_size, gender, neutering|
  search_boarding_modal.modal_search(address, pet_size, gender, neutering)
end

Entao("verifico que a paginação apresenta os resultados com sucesso") do
  error_result = search_result.pagination_validation
  if error_result.empty?
    expecting(true, true, 'Verifica a paginação', 3)
  else
    expecting(false, true, "Erro na paginação. #{error_result} ", 3)
  end

end

E("verifico que o google map apresenta os resultados dos anfitriões encontrados na área de busca") do
  expecting(search_result.check_google_map, true, "Verifica que os resultados apareceram no mapa ao clicar em 'buscar aqui'")
end

E("verifico que entre os resultados posso encontrar badges, informações de disponibilidade e stories dos anfitriões") do
  expecting(search_result.pagination_additional_validation, true, 'Verifica informações complementares')
end

E("altero a ordenação dos resultados para {string}") do | sorting_by |
  search_result.sorting sorting_by
end

E("acesso um anfitrião aleatório") do
  search_result.go_to_random_host
end

E("acesso um anfitrião com stories") do
  search_result.select_host_with_stories
end

E("acesso um anfitrião com avaliações") do
  search_result.select_host_with_rating
end

E("acesso um anfitrião com recomendações") do
  search_result.select_host_with_recommendations
end


E("que acesso o perfil de um anfitrião aleatório através de uma busca") do | table |
  table.hashes.each do |row|
    if MOBILE
      step "efetuo uma busca por um anfitrião no endereço '#{row[:address]}' e com filtros adicionais 'não'"
    else
      step "efetuo uma busca por um anfitrião no endereço '#{row[:address]}' com o tamanho 'random', ambiente 'random', e com filtros adicionais 'não'"
    end
    step "acesso um anfitrião aleatório"
  end
end

E("que acesso o perfil de um anfitrião com stories através de uma busca") do | table |
  table.hashes.each do |row|
    if MOBILE
      step "efetuo uma busca por um anfitrião no endereço '#{row[:address]}' e com filtros adicionais 'não'"
    else
      step "efetuo uma busca por um anfitrião no endereço '#{row[:address]}' com o tamanho 'random', ambiente 'random', e com filtros adicionais 'não'"
    end
    step 'acesso um anfitrião com stories'
  end
end


E("que acesso o perfil de um anfitrião que tenha avaliações através de uma busca") do | table |
  table.hashes.each do |row|
    if MOBILE
      step "efetuo uma busca por um anfitrião no endereço '#{row[:address]}' e com filtros adicionais 'não'"
    else
      step "efetuo uma busca por um anfitrião no endereço '#{row[:address]}' com o tamanho 'random', ambiente 'random', e com filtros adicionais 'não'"
    end
    step 'acesso um anfitrião com avaliações'
  end
end
