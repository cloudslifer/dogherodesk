Dado("faço meu cadastro com: {string}, {string}, {string}, {string}, {string}, {string}, {string}") do | email, name, last_name, phone, password, how_met, whats_notification |
  step 'abro o modal de login'
  step 'clico na opção de registrar um novo usuário'
  register_modal.register_user(email, name, last_name, phone, password, how_met, whats_notification)
end

