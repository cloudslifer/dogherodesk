Entao("valido que as imagens do perfil são apresentadas corretamente") do
  expecting(host_profile_pictures.check_host_pictures, true, 'Valida que as imagens são apresentadas corretamente no perfil do usuário')
end

Entao("valido que os stories são apresentados com sucesso") do
  expecting(host_profile_pictures.check_stories, true, 'Valida que os stories são apresentados com sucesso')
end

Entao("valido que os dados sobre o anfitrião e sua hospedagem são apresentados corretamente") do
  result = host_profile_user_info.check_card_profile

  if result == '[]'
    expecting(true, true, 'Valida que o card do perfil do anfitrião está correto')
  else
    expecting(false, true, "Erro ao validar card do perfil do usuário nos campos: #{result}")
  end

  expecting(host_profile_user_info.check_about_description, true, 'Valida a apresentação da descriçao sobre o anfitrião e seu respectivo modal')
  expecting(host_profile_user_info.check_about_skills, true, 'Valida a apresentação das habilidades do anfitrião e seu respectivo modal')
  expecting(host_profile_user_info.check_about_boarding_description, true, 'Valida a apresentação da descrição sobre a hospedagem e seu respectivo modal')
  expecting(host_profile_user_info.check_about_boarding_extra_info, true, 'Valida a apresentação das informações adicionais sobre a hospedagem e seu respectivo modal')
  if host_profile_user_info.has_achievements
    expecting(host_profile_user_info.check_achievements, true, 'Verifica as conquistas do anfitrião')
  end
  if host_profile_user_info.has_pets
    expecting(host_profile_user_info.check_pets, true, 'Verifica os pets do anfitrião')
  end
  expecting(host_profile_user_info.check_preferences, true, 'Checa se as preferências do usuário são apresentadas')
  expecting(host_profile_user_info.check_map, true, 'Verifica o mapa')
  expecting(host_profile_user_info.check_availabibility, true, 'Checa o calendário de disponibilidade')

  unless MOBILE
    expecting(host_profile_user_info.check_host_form, true, 'Valida a sessão de formulário do anfitrião')
  end

end

Entao("valido o menu com os dados do anfitrião e botoões de ação") do
  expecting(host_profile_user_info.check_menu, true, 'Valida o menu superior')
end

Entao("valido as avaliações do anfitrião") do
  expecting(host_profile_reviews.check_reviews(true), true, 'Valida as reviews do host')
  expecting(host_profile_reviews.check_modal_pages(true), true, 'Valida o modal de reviews')
end

Entao("defino com sucesso datas para a reserva utilizando o menu superior {string}") do | top_menu |
  expecting(host_profile.set_boarding_date(top_menu), true, 'define datas para a hospedagem')
end