Dado("faço o login com meu email {string} e senha {string}") do |login, password|
  step 'abro o modal de login'
  login_modal.fill_login login, password
  tracing "preenchendo login com usuário: #{login} e senha: #{password}", true
  login_modal.do_login

end

Então("a mensagem de dados incorretos é apresentada") do
  expecting(login_modal.is_invalid_login, true, 'Login inválido')
end

Então("clico na opção de registrar um novo usuário") do
  login_modal.go_to_register
end


Dado("estou logado com sucesso") do |table|
  table.hashes.each do |row|
    step "faço o login com meu email '#{row[:email]}' e senha '#{row[:password]}'"
    step 'o meu acesso é efetuado com sucesso'
  end
end


