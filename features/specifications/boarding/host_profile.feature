#language: pt
#encoding: utf-8

@FullSmoke
@Boarding
@HostProfile

Funcionalidade: Verificar o perfil do anfitrião

  - Narrativa:
  Verificar perfil do anfitrião a fim de garantir que o mesmo seja apresentado corretamente para futuras reservas

  Cenário: Validar imagens
    Dado que estou na página de busca por anfitrião
    Quando que acesso o perfil de um anfitrião aleatório através de uma busca
    | address |
    | random  |
    Entao valido que as imagens do perfil são apresentadas corretamente

  Cenário: Validar stories
    Dado que estou na página de busca por anfitrião
    Quando que acesso o perfil de um anfitrião com stories através de uma busca
      | address |
      | random  |
    Entao valido que os stories são apresentados com sucesso

  Cenário: Validar dados do anfitrião
    Dado que estou na página de busca por anfitrião
    Quando que acesso o perfil de um anfitrião aleatório através de uma busca
      | address |
      | random  |
    Entao valido que os dados sobre o anfitrião e sua hospedagem são apresentados corretamente


  Cenário: Validar avaliações dos anfitriões
    Dado que estou na página de busca por anfitrião
    Quando que acesso o perfil de um anfitrião que tenha avaliações através de uma busca
      | address |
      | random  |
    Entao valido as avaliações do anfitrião


  @Desktop
  @ignoreMobile
  Cenário: Validar menu superior
    Dado que estou na página de busca por anfitrião
    Quando que acesso o perfil de um anfitrião aleatório através de uma busca
      | address |
      | random  |
    Entao valido o menu com os dados do anfitrião e botoões de ação


  @Desktop
  @ignoreMobile
  Esquema do Cenario: Definir datas para a reserva
    Dado que estou na página de busca por anfitrião
    Quando que acesso o perfil de um anfitrião aleatório através de uma busca
      | address |
      | random  |
    Entao defino com sucesso datas para a reserva utilizando o menu superior "<top_menu>"

    Exemplos:
    | top_menu |
    | sim      |
    | não      |



