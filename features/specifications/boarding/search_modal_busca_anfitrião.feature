#language: pt
#encoding: utf-8

@FullSmoke
@Boarding
@Search
@SearchModalBoarding


Funcionalidade: Busca por anfitrião - Modal

  - Narrativa:
  Efetuar busca a fim de escolher um anfitrião para meu pet

  @Search
  @Desktop
  @Mobile
  Esquema do Cenario: Cenario: Validar busca pelo modal não logado
    Dado que estou na página de busca por anfitrião
    Quando efetuo uma busca por um anfitrião pelo modal no endereço "<address>" com o tamanho "<pet_size>", gênero "<gender>", e castrado "<neutering>"
    Entao valido os resultados apresentados

  Exemplos:
    | address | pet_size | gender | neutering |
    | random  | random   | macho  | sim       |
    | outro_sp| default  | fêmea  | não       |


  @Desktop
  @Mobile
  Esquema do Cenário: Validar busca pelo modal logado
    Dado que estou logado na página de busca por anfitrião
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    Quando efetuo uma busca por um anfitrião pelo modal no endereço "<address>" com o tamanho "<pet_size>", gênero "<gender>", e castrado "<neutering>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | pet_size | gender | neutering |
      | random  | random   | macho  | sim       |
      | outro_sp| default  | fêmea  | não       |

  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca complementar pelo modal não logado
    Dado que efetuo uma busca na tela inicial por um anfitrião
      | address   | pet_size |
      | <address> | <pet_size> |
    Quando efetuo uma busca por um anfitrião pelo modal no endereço "<address_modal>" com o tamanho "<pet_size_modal>", gênero "<gender_modal>", e castrado "<neutering_modal>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | pet_size | address_modal | pet_size_modal | gender_modal | neutering_modal |
      | random  | random   | random        | random         | random       | random          |

  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca complementar pelo modal logado
    Dado que estou logado na tela inicial
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    E que efetuo uma busca na tela inicial por um anfitrião
      | address   | pet_size   |
      | <address> | <pet_size> |
    Quando efetuo uma busca por um anfitrião pelo modal no endereço "<address_modal>" com o tamanho "<pet_size_modal>", gênero "<gender_modal>", e castrado "<neutering_modal>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | pet_size | address_modal | pet_size_modal | gender_modal | neutering_modal |
      | random  | random   | random        | random         | random       | random          |


  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca complementar pelo modal não logado
    Dado que efetuo uma busca na tela inicial por um anfitrião
      | address   |
      | <address> |
    Quando efetuo uma busca por um anfitrião pelo modal no endereço "<address_modal>" com o tamanho "<pet_size_modal>", gênero "<gender_modal>", e castrado "<neutering_modal>"
    Entao valido os resultados apresentados

    Exemplos:
      | address  | address_modal | pet_size_modal | gender_modal | neutering_modal |
      | random   | random        | random         | random       | random          |


  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca complementar pelo modal logado
    Dado que estou logado na tela inicial
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    E que efetuo uma busca na tela inicial por um anfitrião
      | address   |
      | <address> |
    Quando efetuo uma busca por um anfitrião pelo modal no endereço "<address_modal>" com o tamanho "<pet_size_modal>", gênero "<gender_modal>", e castrado "<neutering_modal>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | address_modal | pet_size_modal | gender_modal | neutering_modal |
      | random  | random        | random         | random       | random          |

