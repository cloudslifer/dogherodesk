#language: pt
#encoding: utf-8

@FullSmoke
@Boarding
@Home
@Search
@SearchHomeBoarding
Funcionalidade: Busca por anfitrião na homepage

  - Narrativa:
  Efetuar busca a fim de escolher um anfitrião para meu pet

  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca deslogado
    Dado que estou na tela inicial
    Quando efetuo uma busca por um anfitrião com o endereço "<endereço>" sem os pets "<pets>"
    E fecho o modal da tela de busca
    Entao valido os resultados apresentados

    Exemplos:
    | endereço  | pets   |
    | random    | random |
    | centro_sp | default |
    | random    | +40 kg  |

  @Desktop
  @ignoreMobile
  Esquema do Cenario: Validar busca logado
    Dado que estou logado na tela inicial
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    Quando efetuo uma busca por um anfitrião com o endereço "<endereço>" sem os pets "<pets>"
    E fecho o modal da tela de busca
    Entao valido os resultados apresentados

    Exemplos:
      | endereço  | pets     |
      | outro_sp  | random   |
      | random    | até 5 kg |
      | random    | 10-20 kg, 20-40 kg |


  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca deslogado
    Dado que estou na tela inicial
    Quando efetuo uma busca por um anfitrião com o endereço "<endereço>"
    E fecho o modal da tela de busca
    Entao valido os resultados apresentados

    Exemplos:
      | endereço  |
      | random    |
      | centro_sp |
      | random    |

  @Mobile
  @ignoreDesktop
  Esquema do Cenario: Validar busca logado
    Dado que estou logado na tela inicial
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    Quando efetuo uma busca por um anfitrião com o endereço "<endereço>"
    E fecho o modal da tela de busca
    Entao valido os resultados apresentados

    Exemplos:
      | endereço  |
      | outro_sp  |
      | random    |
      | random    |
