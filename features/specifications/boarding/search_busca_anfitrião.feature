#language: pt
#encoding: utf-8


@FullSmoke
@Boarding
@Search
@SearchBoarding
Funcionalidade: Busca por anfitrião

  - Narrativa:
  Efetuar busca a fim de escolher um anfitrião para meu pet

  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "<address>" com o tamanho "<pet_size>", ambiente "<environment>", e com filtros adicionais "não"
    Entao valido os resultados apresentados

  Exemplos:
    | address | pet_size | environment |
    | random  | random   | random       |

  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca logado
    Dado que estou logado na página de busca por anfitrião
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "<address>" com o tamanho "<pet_size>", ambiente "<environment>", e com filtros adicionais "não"
    Entao valido os resultados apresentados

    Exemplos:
      | address | pet_size | environment |
      | random  | random   | random       |
  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca com filtros adicionais
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    E efetuo uma busca por um anfitrião no endereço "<address>" com o tamanho "<pet_size>", ambiente "<environment>", e com filtros adicionais "sim"
    Quando preencho os filtros adicionais: área externa "<external_area>", pets do anfitrião "<host_pets>", outros "<others>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | pet_size | environment | external_area | host_pets | others |
      | random  | random   | random       | random        | random    | random |

  @Desktop
  @ignoreMobile
  Esquema do Cenário: Validar busca com filtros adicionais logado
    Dado que estou logado na página de busca por anfitrião
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    E fecho o modal da tela de busca
    E efetuo uma busca por um anfitrião no endereço "<address>" com o tamanho "<pet_size>", ambiente "<environment>", e com filtros adicionais "sim"
    Quando preencho os filtros adicionais: área externa "<external_area>", pets do anfitrião "<host_pets>", outros "<others>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | pet_size | environment | external_area | host_pets | others |
      | random  | random   | random       | random        | random    | random |

  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "<address>" e com filtros adicionais "não"
    Entao valido os resultados apresentados

    Exemplos:
      | address |
      | random  |

  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca logado
    Dado que estou logado na página de busca por anfitrião
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "<address>" e com filtros adicionais "não"
    Entao valido os resultados apresentados

    Exemplos:
      | address |
      | random  |

  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca com filtros adicionais
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    E efetuo uma busca por um anfitrião no endereço "<address>" e com filtros adicionais "sim"
    Quando preencho os filtros adicionais: ordenação "<sorting>", tamanho do pet "<pet_size>", ambiente "<environment>", área externa "<external_area>", pets do anfitrião "<host_pets>", outros "<others>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | sorting | pet_size | environment | external_area | host_pets | others |
      | random  | random  | random   | random       | random        | random    | random |

  @Mobile
  @ignoreDesktop
  Esquema do Cenário: Validar busca com filtros adicionais logado
    Dado que estou logado na página de busca por anfitrião
      | email                  | password |
      | testeqa@mailinator.com | 123456aa |
    E fecho o modal da tela de busca
    E efetuo uma busca por um anfitrião no endereço "<address>" e com filtros adicionais "sim"
    Quando preencho os filtros adicionais: ordenação "<sorting>", tamanho do pet "<pet_size>", ambiente "<environment>", área externa "<external_area>", pets do anfitrião "<host_pets>", outros "<others>"
    Entao valido os resultados apresentados

    Exemplos:
      | address | sorting | pet_size | environment | external_area | host_pets | others |
      | random  | random  | random   | random       | random        | random    | random |

  @Desktop
  @ignoreMobile
  @Pagination
  Cenário: Validar paginação
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "centro_sp" com o tamanho "default", ambiente "default", e com filtros adicionais "não"
    Entao verifico que a paginação apresenta os resultados com sucesso

  @Desktop
  @ignoreMobile
  @GoogleMaps
  Cenário: Validar google maps
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "centro_sp" com o tamanho "default", ambiente "default", e com filtros adicionais "não"
    Entao verifico que o google map apresenta os resultados dos anfitriões encontrados na área de busca

  @Desktop
  @ignoreMobile
  @ExtraInfo
  Cenário: Validar informações adicionais Badges, disponibilidade, stories
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "centro_sp" com o tamanho "default", ambiente "default", e com filtros adicionais "não"
    Entao verifico que entre os resultados posso encontrar badges, informações de disponibilidade e stories dos anfitriões

  @Desktop
  @ignoreMobile
  @Sorting
  Esquema do Cenario: Cenário: Validar informações adicionais - Badges, disponibilidade, stories
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "centro_sp" com o tamanho "default", ambiente "default", com filtros adicionais "não" e contemplando fins de semana
    E altero a ordenação dos resultados para "<sorting>"
    Entao valido os resultados apresentados

    Exemplos:
    | sorting |
    | random  |

  @Mobile
  @ignoreDesktop
  @Pagination
  Cenário: Validar paginação
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "centro_sp" e com filtros adicionais "não"
    Entao verifico que a paginação apresenta os resultados com sucesso


    @this
  @Mobile
  @ignoreDesktop
  @ExtraInfo
  Cenário: Validar informações adicionais Badges, disponibilidade, stories
    Dado que estou na página de busca por anfitrião
    E fecho o modal da tela de busca
    Quando efetuo uma busca por um anfitrião no endereço "centro_sp", com filtros adicionais "não" e contemplando fins de semana
    Entao verifico que entre os resultados posso encontrar badges, informações de disponibilidade e stories dos anfitriões


