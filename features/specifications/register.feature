#language: pt
#encoding: utf-8

@FullSmoke
@Register
Funcionalidade: Cadastrar novo usuário

  - Narrativa:
  Eu como usuário quero poder me registrar a fim de poder conversar com anfitriões, fechar estadias, entre outras opções que exigem autenticação

  Esquema do Cenário: Validar registro
    Dado que estou na tela inicial
    E faço meu cadastro com: "<email>", "<name>", "<last_name>", "<phone>", "<password>", "<how_met>", "<whats_notification>"
    Então valido que meu cadastro foi efetuado com sucesso
    E faço o logout

  Exemplos:
    | email   | name   | last_name | phone  | password | how_met | whats_notification |
    | random  | random | random    | random | default  | random  | random             |

