#language: pt
#encoding: utf-8

@FullSmoke
@Login
Funcionalidade: Login

  - Narrativa:
  Fazer login para garantir que apenas usuários válidos acessem seus dados pessoais no sistema

  Cenário: Validar login com usuário existente e logout
    Dado que estou na tela inicial
    E faço o login com meu email "testeqa@mailinator.com" e senha "123456aa"
    Então o meu acesso é efetuado com sucesso
    E faço o logout

  Esquema do Cenário: Cenário: Validar que login não é efetuado com sucesso com usuários inválidos
    Dado que estou na tela inicial
    E faço o login com meu email "<email>" e senha "<senha>"
    Então a mensagem de dados incorretos é apresentada

    Exemplos:
    | email               | senha          |
    | asx#s               |                |
    |                     | hsd923         |
    | as@sas.com          |                |
    | abc@mailinator.com  | 12345ABC       |