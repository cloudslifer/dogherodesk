#language: pt
#encoding: utf-8

@MeusHerois
Funcionalidade: Meus Herois

  - Narrativa:
  Acessar a tela de Meus Heróis garantindo assim a possibilidade de conversar e escolher entre os heróis disponíveis

  Contexto: Acessar DogHero
    Dado que estou na tela inicial

  Cenário: Verificar meus heróis
    E faço o login com meu email "testeqa@mailinator.com" e senha "123456aa"
    Quando acesso a tela de meus heróis
    Então verifico que minha quantidade total de heróis é "5"

  Esquema do Cenário: Contatar um herói
    E estou logado com sucesso na tela de meus heróis
    | email    | password  |
    | <email>  | <password>  |
    Quando eu contato o herói "<nome_heroi>"
    Então sou direcionado para a tela de contato

    Exemplos:
    | nome_heroi | email                    | password |
    | Vera       | testeqa@mailinator.com   | 123456aa |