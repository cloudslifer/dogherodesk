module SpecPages
  class PreEncontro < SitePrism::Page

    def is_pre_encontro_page
      sleep 5
      switch_to_new_tab
      is_visible_el 'pre_encontro.is_pre_encontro_page'
    end


  end
end