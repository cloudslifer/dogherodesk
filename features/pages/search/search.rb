module SpecPages
  class Search < SitePrism::Page


  def is_search_page
    wait_page
    using_wait_time 15 do
      is_visible_el 'search.title'
    end

  end

  def search_main_text_validation
    is_search_page

    assertion = []

    assertion << compare_text_el('search.title', get_search_text_by_country('title'))
    assertion << check_page_has_text(get_search_text_by_country('states_with_hosts'))
    assertion << check_page_has_text(get_search_text_by_country('search_result_found_label'))
    unless MOBILE
      assertion << check_page_has_text(get_search_text_by_country('address_label'))
      assertion << check_page_has_text(get_search_text_by_country('dog_size_label'))
      assertion << check_page_has_text(get_search_text_by_country('environment_type_label'))
      assertion << check_page_has_text(get_search_text_by_country('host_date_label'))
      assertion << check_page_has_text(get_search_text_by_country('price_range_label'))
      assertion << check_page_has_text(get_search_text_by_country('sorting_label'))
    end
    assertion.include? false
  end


  def search(address, pet_size, environment, has_extra_options, weekend=false)
    search_boarding_modal.close_modal

    fill_address address

    select_environment_type environment

    select_available_date_checkin weekend
    select_available_date_checkout weekend


    select_pet_size pet_size

    #choose_price_range

    has_extra_options.downcase!
    if has_extra_options == 'no' || has_extra_options == 'não'
      make_search
    end

    re_do_search address, pet_size, environment
  end

  def re_do_search address, pet_size, environment
    $address_search = address
    $pet_size_search = pet_size
    $environment_search = environment
    $has_extra_options_search = 'no'
  end


  def search_mobile address, has_extra_options, weekend=false
    search_boarding_modal.close_modal
    fill_address address
    select_available_date_checkin weekend
    select_available_date_checkout weekend

    has_extra_options.downcase!
    if has_extra_options == 'no' || has_extra_options == 'não'
      make_search
    end

  end


  def fill_address address
    if is_visible_el 'search.search_search_address_clear'
      click_el 'search.search_search_address_clear'
    end
    send_keys_el 'search.search_search_address', random_data(address, 'address')
    set_address_autocomplete
  end

  def is_google_autocomplete_address_displayed
    sleep 4
    is_visible_el 'search.search_set_address'
    unless is_visible_el 'search.search_set_address'
      log_error 'Autocomplete do google não é apresentado'
    end
  end

  def set_address_autocomplete
    wait_page
    click_el 'search.search_click_widget'
    is_google_autocomplete_address_displayed
    click_random_with_text_el 'search.search_set_address'
    sleep 2
  end

  def select_available_date_checkin first=false
    click_el 'search.search_open_checkin'
    elements = find_all_els 'search.search_available_date'
    if first
      date = elements[0]
    else
      date = elements.sample
    end
    @@selected_checkin = date.text.to_i
    date.click
  end

  def select_available_date_checkout last=false
    elements = find_all_els 'search.search_available_date'

    if last
      elements[elements.size-1].click
    else
      available_days = []
      elements.each do | el |
        day = el.text.to_i
        if @@selected_checkin >= 29
          click_el 'search.search_active_date'
          break
        elsif day > @@selected_checkin
          available_days << el
        end
      end

      if available_days.size > 0
        available_days.sample.click
      end
    end

  end


  def select_environment_type environment_type
    select_random_default_or_by_text environment_type, 'search.search_environment_type', 'environment type'
  end

  def select_pet_size pet_size
    select_random_default_or_by_text pet_size, 'search.search_pet_size', 'pet size'
  end

  def select_random_default_or_by_text option, element_finder, description
    elements = find_all_els element_finder
    if option == 'random'
      bool = [true, false]
      elements.each do | el |
        if bool.sample
          click_solved_el el, description
        end
      end
    elsif option != 'default'
      option = option.split(",")
      option.each do | text |
        elements.each do | el |
          if get_text_solved_el(el, description).strip == text.strip
            click_solved_el el, description
          end
        end
      end

    end

  end


  def choose_price_range
    click_el 'search.search_lower_price_picker'
    click_el 'search.search_higher_price_picker'
    click_el 'search.search_price_picker'

  end

  def extra_options external_area, has_dogs, other_options
    click_el 'search.search_additional_filters'

    select_random_default_or_by_text external_area, 'search.search_additional_filters_external_area', 'external area'

    select_random_default_or_by_text has_dogs, 'search.search_additional_filters_has_dogs', 'has dogs'

    select_random_default_or_by_text other_options, 'search.search_additional_filters_other_options', 'other options'


    click_el 'search.search_additional_filters_apply'
    make_search
  end

  def extra_options_mobile sorting, pet_size, environment, external_area, has_dogs, other_options
    click_el 'search.search_additional_filters'

    search_result.sorting sorting

    select_random_default_or_by_text pet_size, 'search.search_additional_pet_size', 'pet size'

    select_random_default_or_by_text environment, 'search.search_additional_environment_type', 'environment'

    select_random_default_or_by_text external_area, 'search.search_additional_filters_external_area', 'external area'

    select_random_default_or_by_text has_dogs, 'search.search_additional_filters_has_dogs', 'has dogs'

    select_random_default_or_by_text other_options, 'search.search_additional_filters_other_options', 'other options'


    click_el 'search.search_additional_filters_apply'
    make_search
  end


  def make_search
    click_el 'search.search_make_search'
  end


  private

  def get_search_text_by_country txt
    search_txts = YAML.load_file('features/pages/search/search_texts.yml')
    search_txts[txt][COUNTRY]
  end

  end
end