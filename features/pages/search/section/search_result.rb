module SpecPages
  class SearchResult < SitePrism::Page

    def valid_result
    assertion = {}

      wait_page
      if is_visible_el 'search.result_card_profile'
        puts 'seeing card profile'
        els = find_all_els 'search.result_card_profile'
        counter = 0
        els.each do | element |

          assertion["main_info_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_main_info')
          puts 'main_info' + counter.to_s
          assertion["profile_name_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_name')
          puts 'profile_name' + counter.to_s
          assertion["host_description_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_host_description')
          puts 'host_description' + counter.to_s
          assertion["location_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_location')
          puts 'location' + counter.to_s
          assertion["price_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_price')
          puts 'price' + counter.to_s
          assertion["favorite_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_favorite')
          puts 'favorite' + counter.to_s
          assertion["thumb_profile_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_thumb_profile')
          puts 'thumb_profile' + counter.to_s
          assertion["highlights_#{counter}"] = is_visible_child_el(element, 'search.result_card_profile_highlights')
          puts 'highlights' + counter.to_s

          counter+=1
        end

        assertion['found_qty'] = is_visible_el 'search.result_qty_found'
        puts 'found qty'


        failed_assertion = []

        assertion.each do |key, value|
          if value == false
            failed_assertion << key
          end
        end

        failed_assertion.to_s
      else
        log_error 'Nenhum card de busca foi apresentado'
      end

    end

    def go_to_random_host
      wait_page
      click_random_el 'search.result_card_profile'
      wait_page
      switch_to_new_tab
    end

    def valid_result_content
      assertion = {}
      els = find_all_els 'search.result_card_profile'
      counter = 0
      els.each do | element |
        assertion["profile_name_#{counter}"] = has_text_solved_el element, 'search.result_card_profile_name'
        assertion["host_description_#{counter}"] = has_text_solved_el element, 'search.result_card_profile_host_description'
        assertion["location_#{counter}"] = has_text_solved_el element, 'search.result_card_profile_location'
        if get_text_solved_el(element, 'location').downcase.include? 'a nan km'
          log_error 'Km não está sendo exibida na localização do anfitrião'
        end
        assertion["price_#{counter}"] = has_text_solved_el element, 'search.result_card_profile_price'

        if is_visible_child_el element, 'search.result_card_profile_review'
          assertion["profile_review_#{counter}"] = has_text_solved_el element, 'search.result_card_profile_review'
        else
          assertion["profile_about_#{counter}"] = has_text_solved_el element, 'search.result_card_profile_about'
        end

        counter+=1
      end

      failed_assertion = []

      assertion.each do |key, value|
        unless value
          failed_assertion << key
        end
      end

      failed_assertion.to_s

    end

    def result_found
      search_txts = YAML.load_file('features/pages/search/search_texts.yml')
      qty_text = search_txts['search_result_found_label'][COUNTRY]

      wait_page
      if is_visible_el 'search.result_qty_found'
        el = get_text_el 'search.result_qty_found'
        el.slice! qty_text
        results = el.strip!
        results

      else
        log_error 'Não foi apresentada a mensagem de resultados encontrados'
      end

    end

    def pagination_validation
      results = result_found.to_i

      if results > 40

        elements = find_all_els 'search.result_pagination'

        valid_result_array = []
        for i in 0..2 do
          page_to_be_clicked = elements[i].text
          click_solved_el elements[i], "Página #{page_to_be_clicked}"
          wait_page

          current_page = get_text_el 'search.result_active_page'
          if current_page.strip.include? page_to_be_clicked.strip
            sleep 1
            check_banners
            valid_result_array << valid_result
          else
            log_error "Página atual deveria ser #{page_to_be_clicked} porém é #{current_page}"
          end
        end

        valid_result_array.delete_if{|error| error == '[]'}
        valid_result_array

      end
    end


    def pagination_additional_validation
      results = result_found.to_i

      availability = false
      badges = false
      stories = false

      if results < 50
        search.search($address_search, $pet_size_search, $environment_search, $has_extra_options_search)
      end

      elements = find_all_els 'search.result_pagination'

      for i in 0..4 do

        if availability && badges && stories
          break
        else
          availability = check_availability
          badges = check_badges
          stories = check_stories

          click_solved_el elements[i], 'Clicando na página'
          wait_page
        end
      end

      unless availability
        log_error 'Nenhuma informação de disponibilidade foi encontrada após uma busca por 5 páginas'
      end

      unless badges
        log_error 'Nenhuma badges foi encontrada após uma busca por 5 páginas'
      end

      unless stories
        log_error 'Nenhum story foi encontrado após uma busca por 5 páginas'
      end

      true

    end

    def select_host_with_stories
      select_host_with 'search.stories_open'
    end

    def select_host_with_rating
      results = result_found.to_i
      if results < 9 && !is_visible_el('search.rating_stars')
        search.search($address_search, $pet_size_search, $environment_search, $has_extra_options_search)
      end

      pages = find_all_els 'search.result_pagination'

      found = false
      for i in 0..2 do
        wait_page

        if is_visible_el 'search.rating_stars'
          stars = find_all_els('search.rating_stars')
          stars.each do | star|
            profile = find_parent_el star, 'search.result_card_profile'

            el = find_child_el(profile, 'search.rating_qty')
            if rating_number(el.text) > 3
              click_solved_el profile, 'Perfil do anfitrião'
              found = true
              break
            end
          end
        end

        if found
          break
        else
          click_solved_el pages[i], 'Clicando na página'
          wait_page
        end
      end

      if found
        wait_page
        switch_to_new_tab
      else
        tracing_error 'Nenhum anfitrião foi encontrado nos parâmetros esperados'
      end


    end

    def rating_number txt
      txt.delete('(').delete(')').delete('avaliações').strip.to_i
    end
    def select_host_with el_condition
      results = result_found.to_i
      if results < 10 && !is_visible_el(el_condition)
        search.search($address_search, $pet_size_search, $environment_search, $has_extra_options_search)
      end

      elements = find_all_els 'search.result_pagination'

      found = false
      for i in 0..2 do
        wait_page

        if is_visible_el el_condition

          el = find_parent_el(find_all_els(el_condition).sample, 'search.result_card_profile')
          click_solved_el el, 'Perfil do anfitrião'

          found = true
        end

        if found
          break
        else
          click_solved_el elements[i], 'Clicando na página'
          wait_page
        end
      end

      if found
        wait_page
        switch_to_new_tab
      else
        tracing_error 'Nenhum anfitrião foi encontrado nos parâmetros esperados'
      end
    end


    def check_banners
      unless is_visible_el 'search.result_banner'
        log_error 'Banners não são apresentados entre as buscas'
      end
      is_visible_el 'search.result_banner'
    end

    def check_google_map
      wait_page

      if is_visible_el 'search.search_map'
        click_el 'search.search_map_search_area'
        is_visible_el 'search.search_map'
      else
        log_error 'Google map não é apresentado nas pesquisas'
      end
    end

    def check_availability
      is_visible_el 'search.result_card_profile_availability'

    end

    def check_badges
      is_visible_el 'search.result_card_profile_badges'
    end

    def check_stories
      if is_visible_el 'search.stories_open'
        click_first_el 'search.stories_open'
        if is_visible_el 'search.stories_is_modal_story'

          find_el 'search.stories_info'
          find_el 'search.stories_chat_btn'
          unless MOBILE
            find_el 'search.stories_controls'
          end
          find_el 'search.stories_video'

          click_el 'search.stories_close'
          true
        else
          log_error 'Modal de stories não foi encontrado'
        end
      end
    end

    def sorting sorting_by
      el = 'search.result_sorting'
      if MOBILE
        el = 'search.search_additional_filters_result_sorting'
      end

      if sorting_by == 'random'
        select_random_el el
      else
        select_text_el el, sorting_by
      end
      wait_page
    end








  end
end