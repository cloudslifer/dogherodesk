module SpecPages
  class SearchBoardingModal < SitePrism::Page


    def close_modal
      wait_page
      for i in 1..3 do
        if is_visible_el 'search.modal_search_close_modal'
          click_script_el 'search.modal_search_close_modal'
          unless is_visible_el 'search.modal_search_close_modal'
            break
          end
        end
      end

    end

    def modal_search address, pet_size, gender, neutering
      is_search_modal
      fill_address address
      select_available_date_checkin
      select_available_date_checkout
      select_pet_size pet_size

      select_gender gender
      select_neutering neutering

      make_modal_search
    end


    private

    def is_search_modal
      unless is_visible_el 'search.modal_search'
        log_error 'Modal de busca não foi apresentado'
      end
      is_visible_el 'search.modal_search'
    end


    def make_modal_search
      click_el 'search.modal_pet_do_search_button'
    end

    def fill_address address
      if is_visible_el 'search.modal_search_clear_address'
        click_el 'search.modal_search_clear_address'
      end
      send_keys_el 'search.modal_search_address', random_data(address, 'address')
      set_address_autocomplete

    end

    def select_available_date_checkin
      click_el 'search.modal_date_checkin'
      elements = find_all_els 'search.modal_date_available_date'
      date = elements.sample
      @@selected_checkin = date.text.to_i
      date.click
    end

    def select_available_date_checkout
      elements = find_all_els 'search.modal_date_available_date'
      available_days = []
      elements.each do | el |
        day = el.text.to_i
        if @@selected_checkin >= 29
          click_el 'search.modal_date_active_date'
          break
        elsif day > @@selected_checkin
          available_days << el
        end
      end

      if available_days.size > 0
        available_days.sample.click
      end

    end

    def select_pet_size pet_size
      elements = find_all_els 'search.modal_pet_size'
      if pet_size == 'random'
        bool = [true, false]
        elements.each do | el |
          if bool.sample
            el.click
          end
        end
      elsif pet_size != 'default'
        pet_size = pet_size.split(",")
        pet_size.each do | text |
          elements.each do | el |
            if get_text_solved_el(el, 'pet size').strip == text.strip
              el.click
            end
          end
        end

      end

    end

    def select_neutering neutering
      neutering.downcase!
      if neutering=='random'
        ntrg = ['sim', 'não']

        click_text_el 'search.modal_pet_neutering', ntrg.sample
      else
        click_text_el 'search.modal_pet_neutering', neutering
      end
    end

    def select_gender gender
      gender.downcase!
      if gender=='random'
        gndr = ['macho', 'fêmea']

        click_text_el 'search.modal_pet_gender', gndr.sample
      else
        click_text_el 'search.modal_pet_gender', gender
      end
    end

    def set_address_autocomplete
      click_el 'search.modal_search_click_widget'
      is_google_autocomplete_address_displayed
      click_random_with_text_el 'search.modal_search_set_address'
      sleep 2
    end

    def is_google_autocomplete_address_displayed
      sleep 4
      is_visible_el 'search.modal_search_set_address'
      unless is_visible_el 'search.modal_search_set_address'
        log_error 'Autocomplete do google não é apresentado'
      end
    end


  end
end