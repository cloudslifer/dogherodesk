module SpecPages
  class HostProfileReviews < SitePrism::Page

    def check_reviews rating
      wait_page
      els = find_all_els 'host_profile.reviews_section_cards'
      els.each do |el|
        find_child_el(el,'host_profile.reviews_section_cards_user_image')
        img = get_attr_el(find_child_el(el, 'host_profile.reviews_section_cards_user_image'), 'src', 'user img')
        if img.empty?
          tracing_error 'Imagem do autor da review não apresentada corretamente'
        end

        find_child_el(el,'host_profile.reviews_section_cards_author')
        if rating
          find_child_elements(el,'host_profile.reviews_section_cards_stars')
        end
      end
      true
    end

    def check_modal_pages rating
      click_el 'host_profile.reviews_section_cards_show_more'
      wait_page
      find_el 'host_profile.reviews_section_cards_modal'

      if is_visible_el 'host_profile.reviews_section_cards_modal_available_pages'
        pages = find_all_els 'host_profile.reviews_section_cards_modal_available_pages'
        size = (pages.size) - 1
        if size <0
          size == 1
        end

        for i in 0..size
          click_solved_el pages[i], 'Página'
          check_reviews_modal rating
        end
      else
        check_reviews_modal rating
      end

      click_el 'host_profile.close_modal'
      true
    end

    def check_reviews_modal rating
      wait_page
      els = find_all_els 'host_profile.reviews_section_cards_modal_cards'
      els.each do | el|
        find_child_el(el, 'host_profile.reviews_section_cards_modal_cards_imgs')
        img = get_attr_el(find_child_el(el, 'host_profile.reviews_section_cards_modal_cards_imgs'), 'src', 'user img')
        if img.empty?
          tracing_error 'Imagem do autor da review não apresentada corretamente'
        end
        find_child_el(el, 'host_profile.reviews_section_cards_modal_cards_profile')
        find_child_el(el, 'host_profile.reviews_section_cards_modal_cards_date')
        if rating
          find_child_elements(el, 'host_profile.reviews_section_cards_modal_cards_stars')
        end
      end
    end

  end
end






















