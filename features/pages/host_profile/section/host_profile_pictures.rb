module SpecPages
  class HostProfilePictures < SitePrism::Page


    def check_host_pictures
      result = false
      if MOBILE

        if has_cover
          tracing 'Capa apresentada'
          if check_pictures_gallery
            tracing 'Galeria é apresentada com sucesso'
            if change_imgs_next
              result = true
              tracing 'Interação com as imagens funciona corretamente'
            end
          end
        end

      else

        if has_cover
          tracing 'Capa apresentada'
          if check_mini_images == '[]'
            tracing 'Imagens de miniatura conferem com imagens abertas'
            if check_pictures_gallery
              tracing 'Galeria é apresentada com sucesso'
              if change_imgs_next
                result = true
                tracing 'Interação com as imagens funciona corretamente'
              end
            end
          end
        end

      end

      result

    end

    def has_cover
      is_visible_el 'host_profile.pictures_cover_picture'
    end

    def check_mini_images
      results = {}
      els = find_all_els 'host_profile.pictures_mini_pictures'
      i = 0
      els.each do | el |
        style = get_attr_el el, 'style', 'mini_pictures'
        click_solved_el el, 'mini_picture'
        src = get_attr_el find_el('host_profile.pictures_current_picture'), 'src', 'current_picture'

        link = get_picture_url style
        link2 = get_picture_url src

        status = false

        wait_page

        if link.include?(link2) || link2.include?(link)
          status = true
        end
        results["el_#{i}"] = status
        click_el 'host_profile.pictures_close_gallery'

        i+=1
      end

      failed_assertion = []

      results.each do |key, value|
        if value == false
          failed_assertion << key
        end
      end

      failed_assertion.to_s
    end

    def get_picture_url url
      arr = url.split("?w=")
      arr[0].downcase
    end

    def check_pictures_gallery
      click_el 'host_profile.pictures_other_pictures'
      find_el 'host_profile.pictures_close_gallery'
      find_el 'host_profile.pictures_current_picture'
      find_el 'host_profile.pictures_modal_title'
      find_el 'host_profile.pictures_swiper_prev'
      find_el 'host_profile.pictures_swiper_next'
      true
    end

    def change_imgs_next
      for i in 1..3
      wait_page
      current_link = get_attr_el find_el('host_profile.pictures_current_picture'), 'src', 'current_picture'
      click_el 'host_profile.pictures_swiper_next'
      wait_page
      new_link = get_attr_el find_el('host_profile.pictures_current_picture'), 'src', 'current_picture'
        if current_link == new_link
          log_error 'Imagem não foi trocada com sucesso'
        end
      end

      change_imgs_prev
    end

    def change_imgs_prev
      current_link = get_attr_el find_el('host_profile.pictures_current_picture'), 'style', 'current_picture'
      click_el 'host_profile.pictures_swiper_prev'
      new_link = get_attr_el find_el('host_profile.pictures_current_picture'), 'style', 'current_picture'


      current_link == new_link
    end

    def check_stories
      wait_page
      if is_visible_el 'host_profile.stories_open'
        click_first_el 'host_profile.stories_open'
        if is_visible_el 'host_profile.stories_is_modal_story'

          find_el 'host_profile.stories_info'
          find_el 'host_profile.stories_chat_btn'
          unless MOBILE
            find_el 'host_profile.stories_controls'
          end
          find_el 'host_profile.stories_video'

          click_el 'host_profile.stories_close'
          true
        else
          log_error 'Modal de stories não foi encontrado'
        end
      end
    end


    # Aguardando correção do bug da miniatura da primeira imagem
    #
    def check_current_page
      #'host_profile.pictures_modal_title'
    end

    def check_total_pages
      #'host_profile.pictures_modal_title'
    end




  end
end