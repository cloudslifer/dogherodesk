module SpecPages
  class HostProfileUserInfo < SitePrism::Page

    def check_card_profile
     wait_page
     content = {}

     find_el 'host_profile.card_profile'
     content['profile'] = has_text_el('host_profile.card_profile')
     find_el 'host_profile.card_profile_img'
     content['user_img_url'] =  !get_attr_el(find_el('host_profile.card_profile_img'), 'src', 'profile picture').empty?
     find_el 'host_profile.card_profile_user_name'
     content['user_name'] =  has_text_el('host_profile.card_profile_user_name')

     counter = 0
     unless MOBILE
       els = find_all_els 'host_profile.card_profile_location_breadcrumbs'
       els.each do | el |
         content["location_breadcrumb_#{counter}"] = has_text_solved_el el, 'location breadcrumb'
         counter+=1
       end
     end

     find_el 'host_profile.card_profile_favorite'

     result = []

     content.each do | value, key |
       if value == false
         result << key
       end
     end

     result.to_s
    end

    def check_about_description
      content = []
      find_el 'host_profile.about_description'
      content << has_text_el('host_profile.about_description')

      if is_visible_el 'host_profile.about_description_show_more'
        click_el 'host_profile.about_description_show_more'
        wait_page
        find_el 'host_profile.about_description_modal'
        content << has_text_el('host_profile.about_description_modal')

        close_modal
      end

      content.include?(false) ? false : true
    end

    def check_about_skills
      content = []

      els = find_all_els 'host_profile.about_skills'
      els.each do | el|
        content << has_text_solved_el(el, 'skills')
      end

      if is_visible_el 'host_profile.about_skills_show_more'
        click_el 'host_profile.about_skills_show_more'
        wait_page

        els = find_all_els 'host_profile.about_skills_modal'
        els.each do | el|
          content << has_text_solved_el(el, 'skills')
        end

        close_modal
      end

      content.include?(false) ? false : true
    end

    def check_about_boarding_description
      content = []
      find_el 'host_profile.how_is_boarding_description'
      content << has_text_el('host_profile.how_is_boarding_description')

      if is_visible_el 'host_profile.how_is_boarding_description_show_more'
        click_el 'host_profile.how_is_boarding_description_show_more'
        wait_page
        content << has_text_el('host_profile.how_is_boarding_description_modal')

        close_modal
      end

      content.include?(false) ? false : true
    end

    def check_about_boarding_extra_info
      content = []
      els = find_all_els 'host_profile.how_is_boarding_extra_info'

      els.each do | el|
      content << has_text_solved_el(el, 'extra info')
      end

      if is_visible_el 'host_profile.how_is_boarding_extra_info_show_more'
        click_el 'host_profile.how_is_boarding_extra_info_show_more'
        wait_page

        els = find_all_els 'host_profile.how_is_boarding_extra_info_modal'

        els.each do | el|
          content << has_text_solved_el(el, 'extra info')
        end

        close_modal
      end

      content.include?(false) ? false : true
    end

    def has_achievements
      is_visible_el 'host_profile.achievements'
    end

    def check_achievements

      if has_achievements
        content = []
        achievs = find_all_els 'host_profile.achievements_list'
        achievs.each do | achiev|
          content << has_text_solved_el(achiev, 'achievments')
        end

        if is_visible_el 'host_profile.achievements_show_more'
          click_el 'host_profile.achievements_show_more'
          find_el 'host_profile.achievements_modal'

          achievs = find_all_els 'host_profile.achievements_modal_achievments'
          achievs.each do | achiev|
            content << has_text_solved_el(achiev, 'achievments')
          end
          close_modal
        end
        content.include?(false) ? false : true
      end
    end


    def has_pets
      is_visible_el 'host_profile.pets'
    end

    def check_pets
      if has_pets
       content  = []
       pets = find_all_els 'host_profile.pets_list'
       pets.each do | pet |
         content << has_text_solved_el(pet, 'pets')
       end
       content.include?(false) ? false : true
      end
    end

    def check_preferences
      is_visible_el 'host_profile.preferences'
    end

    def check_map
      find_el 'host_profile.section_map'

      el = find_el 'host_profile.section_map_icon_map'
      parent = find_parent_el el, 'host_profile.section_map_icon_map_parent'
      find_el 'host_profile.section_map_map'

      has_text_solved_el parent, 'localização'

    end

    def check_availabibility
      find_el 'host_profile.availability'
      find_el 'host_profile.availability_calendar'
      find_el 'host_profile.availability_calendar_header'
      find_all_els 'host_profile.availability_extra_info'

      content = []
      els = find_all_els 'host_profile.availability_calendar_index'
      els.each do | el |
        content << has_text_solved_el(el, 'index')
      end

      content = nav_calendar content

      content.include?(false) ? false : true
    end

    def nav_calendar content
      select_random_el 'host_profile.availability_calendar_header_select_month'
      content << check_calendar
      select_random_el 'host_profile.availability_calendar_header_select_year'
      content << check_calendar

      for i in 1..3
        click_el 'host_profile.availability_calendar_header_next_month'
        content << check_calendar
      end

      click_el 'host_profile.availability_calendar_header_previous_month'
      content << check_calendar
      content
    end

    def check_calendar
      find_all_els 'host_profile.availability_calendar_inside_days'
      has_text_el 'host_profile.availability_calendar_inside_days_week_name'
    end

    def set_host_info
      find_el 'host_profile.card_profile_user_name'
      @@host_name = get_text_el 'host_profile.card_profile_user_name'
      find_el 'host_profile.host_form_section_price'
      @@host_price = get_text_el 'host_profile.host_form_section_price'
      @@has_rating = is_visible_el 'host_profile.reviews_section_cards_stars'

    end

    def check_menu
      if MOBILE
        find_el 'host_profile.menu_reservation'
        find_el 'host_profile.menu_conversation'
        find_el 'host_profile.menu_price'
        has_text_el 'host_profile.menu_price'
      else
        wait_page
        set_host_info
        scroll_page_down '650'

        find_el 'host_profile.top_menu'
        find_el 'host_profile.top_menu_host_image'
        find_el 'host_profile.top_menu_host_favorite'

        if @@has_rating
          find_all_els 'host_profile.top_menu_rating_stars'
          find_el 'host_profile.top_menu_rating_qty'
        end

        find_el 'host_profile.top_menu_checkin'
        find_el 'host_profile.top_menu_checkout'
        find_el 'host_profile.top_menu_reservation'
        find_el 'host_profile.top_menu_start_conversation'

        compare_top_menu_info
      end
    end

    def compare_top_menu_info
      find_el 'host_profile.top_menu_host_name'
      name = get_text_el 'host_profile.top_menu_host_name'
      find_el 'host_profile.top_menu_price'
      price = get_text_el 'host_profile.top_menu_price'


      unless name == @@host_name
        log_error "Nome divergente do nome apresentado na página. Nome na página: #{@@host_name}. Nome no menu superior: #{name}"
      end

      unless price == @@host_price
        log_error "Preço divergente do preço apresentado na página. Preço na página: #{@@host_price}. Preço no menu superior: #{price}"
      end

      true
    end

    def check_host_form
      find_el 'host_profile.host_form_section_price'
      find_el 'host_profile.host_form_section_checkin'
      find_el 'host_profile.host_form_section_checkout'
      find_el 'host_profile.host_form_section_start_conversation'
      find_el 'host_profile.host_form_section_reservation'
      find_all_els 'host_profile.host_form_section_extra_info'

      find_el 'host_profile.host_form_section_host_policy'
      click_el 'host_profile.host_form_section_host_policy'
      find_el 'host_profile.host_form_modal_policy'
      res = has_text_el 'host_profile.host_form_modal_policy'
      close_modal
      res
    end

    def close_modal
      click_el 'host_profile.close_modal'
      wait_page
    end


  end
end