module SpecPages
  class HostProfile < SitePrism::Page


    def is_host_profile
      using_wait_time 15 do
        is_visible_el 'host_profile.host_profile'
      end

    end

    def check_host_profile
      assertion = []

      assertion << check_page_has_text(get_hosts_text_by_country('about'))
      assertion << check_page_has_text(get_hosts_text_by_country('preferences'))
      assertion << check_page_has_text(get_hosts_text_by_country('availability'))
      assertion << check_page_has_text(get_hosts_text_by_country('availability_type_free'))
      assertion << check_page_has_text(get_hosts_text_by_country('availability_type_free_with_pets'))
      assertion << check_page_has_text(get_hosts_text_by_country('availability_type_busy'))
      assertion << check_page_has_text(get_hosts_text_by_country('how_many_pets'))
      assertion << check_page_has_text(get_hosts_text_by_country('how_hosting'))
      assertion << check_page_has_text(get_hosts_text_by_country('where_do_i_live'))
      assertion << check_page_has_text(get_hosts_text_by_country('reply_time'))
      assertion << check_page_has_text(get_hosts_text_by_country('reply_rate'))
      assertion << check_page_has_text(get_hosts_text_by_country('price_by_night'))
      assertion << check_page_has_text(get_hosts_text_by_country('host_policy'))
      assertion << check_page_has_text(get_hosts_text_by_country('profile_update'))
      assertion << check_page_has_text(get_hosts_text_by_country('vet_warranty'))
      assertion << check_page_has_text(get_hosts_text_by_country('checkin'))
      assertion << check_page_has_text(get_hosts_text_by_country('checkout'))


      assertion.include?(false) ? false : true
    end



    def go_to_reservation
      click_el 'host_profile.host_form_section_reservation'
    end

    def go_to_reservation_top_menu
      show_top_menu
      click_el 'host_profile.top_menu_reservation'
    end

    def go_to_conversation
      click_el 'host_profile.host_form_section_start_conversation'
    end

    def go_to_conversation_top_menu
      show_top_menu
      click_el 'host_profile.top_menu_start_conversation'
    end

    def show_top_menu
      scroll_page_down '650'
    end


    def set_boarding_date top_menu
      wait_page
      if top_menu == 'yes' || top_menu == 'sim'
        top_menu = true
      else
        top_menu = false
      end

      select_available_date_checkin top_menu
      wait_page
      select_available_date_checkout top_menu
      true
    end

    private

    def select_available_date_checkin top_menu

      el = 'host_profile.host_form_section_checkin'

      if top_menu
        el = 'host_profile.top_menu_checkin'
        show_top_menu
      end

      click_el el
      elements = find_all_els 'host_profile.available_date'
      date = elements.sample
      @@selected_checkin = date.text.to_i
      date.click
    end

    def select_available_date_checkout top_menu
      checkout_el = 'host_profile.host_form_section_checkout'
      if top_menu
        checkout_el = 'host_profile.top_menu_checkout'
        show_top_menu
      end

      click_el checkout_el

      elements = find_all_els 'host_profile.available_date'
      available_days = []
      elements.each do | el |
        day = el.text.to_i
        if @@selected_checkin >= 29
          click_el 'host_profile.active_date'
          break
        elsif day > @@selected_checkin
          available_days << el
        end
      end

      if available_days.size > 0
        available_days.sample.click
      end

    end

    def get_hosts_text_by_country txt
      search_txts = YAML.load_file('features/pages/host_profile/host_profile_texts.yml')
      search_txts[txt][COUNTRY]
    end


  end
end