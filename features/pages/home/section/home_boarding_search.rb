module SpecPages
  class HomeBoardingSearch < SitePrism::Page

    def boarding_search address, pet_size
      home.close_pop_up

      click_el 'home.choose_search_boarding'
      fill_address address
      choose_pet_size pet_size

      make_search
    end

    def boarding_search_mobile address
      home.close_pop_up

      click_el 'home.choose_search_boarding'
      fill_address address

      make_search
    end

    def make_search
      click_el 'home.make_search_boarding'
    end

    private

    def choose_pet_size pet_size
      unless pet_size == 'default'
        click_el 'home.search_boarding_open_pet_size'
        random_boolean = [true, false]
        if pet_size == 'random'

          els = find_all_els 'home.search_boarding_select_pet_size'
          els.each do | el |
            if random_boolean.sample
              click_solved_el el, 'checkbox de seleção de tamanho do pet'
            end
          end

        else
          pet_size = pet_size.split(",")

          pet_size.each do | text |
            els = find_all_els 'home.search_boarding_check_pet_size'
            els.each do |el|
              if get_text_solved_el(el, 'descrição da checkbox').strip == text.strip

                check_box = find_child_elements(el, 'home.search_boarding_select_pet_size')

                click_solved_el check_box[0], 'checkbox de seleção de tamanho do pet'
              end
            end
          end
        end
      end

    end

    def fill_address address
      is_visible_el 'home.search_boarding_fill_address'
      send_keys_el 'home.search_boarding_fill_address', random_data(address, 'address')
      set_address_autocomplete
    end

    def set_address_autocomplete
      click_el 'home.search_boarding_click_widget'
      is_google_autocomplete_address_displayed
      click_random_with_text_el 'home.search_boarding_set_address'
      sleep 2
    end

    def is_google_autocomplete_address_displayed
      sleep 4
      is_visible_el 'home.search_boarding_set_address'
      unless is_visible_el 'home.search_boarding_set_address'
        log_error 'Autocomplete do google não é apresentado'
      end
    end

  end
end