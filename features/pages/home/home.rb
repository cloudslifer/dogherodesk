module SpecPages
  class Home < SitePrism::Page
    @@url = "#{LINKS[ENV['TEST_ENVIRONMENT']]['home']}"



    set_url @@url

    def get_url
      @@url
    end

    def is_home
      set_additional_info
      wait_page
      is_visible_el 'home.check_home_page'
    end

    def open_menu
      click_el 'home.open_menu'
    end

    def open_menu_mobile
      if MOBILE
        click_el 'home.open_menu'
      end
    end

    def is_user_logged
      open_menu_mobile
      bool = is_visible_el 'home.user_profile'
      open_menu_mobile
      bool
    end

    def check_user_name user_name
      wait_page
      is_user_logged
      open_menu_mobile
      is_visible_el 'home.user_name'

      name = get_text_el 'home.user_name'

      name.downcase.include?(user_name.downcase)
    end

    def access_my_heroes_page
      click_el 'home.access_my_heroes'
    end

    def open_login_modal
      if MOBILE
        open_menu_mobile
        click_text_el 'home.open_login', 'Entrar'
      else
        click_el 'home.open_login'
      end
    end

    def do_logout
      wait_page
      unless is_visible_el 'home.do_logout'
        open_menu
      end
      click_text_el 'home.do_logout', 'Sair'
    end

    def check_logged_out
      wait_page
      open_menu_mobile
      is_visible_text_el 'home.open_login', 'Entrar'
    end

    def close_pop_up
      wait_page
      for i in 1..3 do
        if is_visible_el 'home.close_popup'
          click_script_el 'home.close_popup'
          unless is_visible_el 'home.close_popup'
            break
          end
        end
      end
    end

    def go_to_boarding_search

      home_boarding_search.make_search
=begin
      if MOBILE

        #go_to "#{@@url}/busca"
      else
        click_text_el 'home.find_your_hero', 'encontre seu herói'
        click_el 'home.find_your_hero_find_a_host'
      end
=end
    end

    private
    def set_additional_info
      $current_host = Capybara.current_host.to_s
      $ip_host = IPSocket::getaddress(@@url.gsub("http://", ""))
    end






  end
end