module SpecPages
  class LoginModal < SitePrism::Page

    def fill_login(email, password)
      send_keys_el 'login_modal.email', email
      send_keys_el 'login_modal.password', password
    end

    def do_login
      click_el 'login_modal.do_login'
      wait_page
    end

    def is_invalid_login
      is_visible_el 'login_modal.invalid_login_error'
    end

    def go_to_register
      click_el 'login_modal.do_register'
    end
  end
end