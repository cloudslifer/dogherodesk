module SpecPages
  class RegisterModal < SitePrism::Page

    def register_user (email, name, last_name, phone, password, how_met, whats_notification)
      send_keys_el 'register_modal.email', random_data(email, 'email')
      name = random_data(name, 'name')
      $user_name = name
      send_keys_el 'register_modal.first_name', name
      send_keys_el 'register_modal.last_name', random_data(last_name, 'last_name')
      send_keys_el 'register_modal.phone', random_data(phone, 'phone_number')
      send_keys_el 'register_modal.password', default_password(password)

      select_how_met how_met
      select_whatsapp_notification whats_notification

      click_el 'register_modal.finish_register'

    end

    def select_whatsapp_notification whats_notification
      checked = is_checked_el 'register_modal.check_whatsapp_notification'
      if whats_notification == 'sim' || whats_notification == 'yes'
        unless checked
          click_el 'register_modal.check_whatsapp_notification'
        end
      elsif whats_notification == 'random'
        rand = [true, false]
        if rand.sample
          click_el 'register_modal.check_whatsapp_notification'
        end
      else
        if checked
          click_el 'register_modal.check_whatsapp_notification'
        end
      end

    end

    def select_how_met how_met
      if how_met == 'random'
        select_random_el 'register_modal.how_did_you_met'
      else
        select_text_el 'register_modal.how_did_you_met', how_met
      end
    end


























  end
end