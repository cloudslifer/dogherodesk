module SpecPages
  class MeusHerois < SitePrism::Page
    def has_heroes
      is_visible_list_el 'meus_herois.heroes_container'
    end

    def check_heroes_qty qty
      list_size = find_all_els('meus_herois.heroes_container').size

      list_size == qty.to_i

    end

    def choose_hero_by_name name
      all_heroes = find_all_els 'meus_herois.heroes_names'
      name.downcase!
      size = -1
      all_heroes.each do |el|
        size = size + 1
        found_name = el.text.downcase
        if found_name.include? name
          els = find_all_els 'meus_herois.contact_option'
          click_solved_el els[size], 'botão de contatar o heroi'
          break
        end
      end
    end

  end
end